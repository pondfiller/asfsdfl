#! /bin/bash

wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
chsh -s /bin/zsh root
sed -i 's/enforcing/disabled/g' /etc/selinux/config /etc/selinux/config
authconfig --passalgo=sha512 --update

USER_NAME=user
adduser --gecos ""  $USER_NAME
sed -i "s/.*RSAAuthentication.*/RSAAuthentication yes/g" /etc/ssh/sshd_config
sed -i "s/.*PubkeyAuthentication.*/PubkeyAuthentication yes/g" /etc/ssh/sshd_config
sed -i "s/.*PasswordAuthentication.*/PasswordAuthentication no/g" /etc/ssh/sshd_config
sed -i "s/.*AuthorizedKeysFile.*/AuthorizedKeysFile\t\.ssh\/authorized_keys/g" /etc/ssh/sshd_config
sed -i "s/.*PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config
echo "${USER_NAME}      ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
service sshd restart
cp -pr /root/.ssh /home/$USER_NAME/
chown -R $USER_NAME:$USER_NAME /home/$USER_NAME/.ssh
chmod 700 /home/$USER_NAME/.ssh
chmod 600 /home/$USER_NAME/.ssh/authorized_keys

touch /root/post-install-applied.done
reboot